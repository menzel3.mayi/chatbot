# lita-calculator

[![Build Status](https://travis-ci.org/white3/lita-calculator.png?branch=master)](https://travis-ci.org/white3/lita-calculator)
[![Coverage Status](https://coveralls.io/repos/white3/lita-calculator/badge.png)](https://coveralls.io/r/white3/lita-calculator)

TODO: Add a description of the plugin.

## Installation

Add lita-calculator to your Lita instance's Gemfile:

``` ruby
gem "lita-calculator"
```

## Configuration

TODO: Describe any configuration attributes the plugin exposes.

## Usage

TODO: Describe the plugin's features and how to use them.
